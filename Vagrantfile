# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/focal65"

  IP_MASK = "192.168.100."
  DB = 3
  DB_VM_NAME = "psql"

  def create_vm(config, hostname, ip, cpus=1, memory=1024)
    config.vm.define hostname do |host|
      host.vm.network "private_network", ip: ip
      host.vm.hostname = hostname
      # Common to all VMs
      # host.vm.provision :host, :sync_hosts => true
      # host.vm.provision "shell", inline: "apt-get update && apt-get upgrade -y"

      host.vm.provider "virtualbox" do |vm|
        vm.name = "Vagrant_#{hostname}"
        vm.customize ["modifyvm", :id, "--cpus", cpus.to_s]
        vm.customize ["modifyvm", :id, "--memory", memory.to_s]
        vm.customize ["modifyvm", :id, "--audio", "none"]
        end
      yield host if block_given?
    end
  end

  (1..DB).each do |vm_id|
    create_vm(config, "#{DB_VM_NAME}-#{vm_id}", "#{IP_MASK}#{10+vm_id}") do |host|
      if vm_id == DB
        host.vm.provision "ansible" do |ansible|
          ansible.limit = "all"
          ansible.playbook = "playbook.yml"
          ansible.become = true
          ansible.groups = {
            "db_master"       => "#{DB_VM_NAME}-1",
            "db_slave"        => ["#{DB_VM_NAME}-[2:#{DB}]"],
            "all"             => ["#{DB_VM_NAME}-[1:#{DB}]"],
            "all:vars"        => {
              "db_master_ip"  => "#{IP_MASK}#{10+1}",
              # "db_slave_ip#{1..DB-1}" => "#{IP_MASK}#{2..DB+10}",
            }
          }
        end
      end
    end
  end
end
