Role Name
=========

A brief description of the role goes here.

Requirements
------------

Ansible modules:
  - community.general
  - community.postgresql

Role Variables
--------------

All variables are in default/main.yml

Dependencies
------------

No dependencies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - postgresql
License
-------

BSD

